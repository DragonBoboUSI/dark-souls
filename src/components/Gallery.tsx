import { Fade, Flex, Grid, Image, Text } from "@chakra-ui/react";
import { FC, useState } from "react";
import { DesignPrincipleType } from "assets/data";
import { LeftArrowIcon, RightArrowIcon } from "./Icons";

type GalleryProps = {
    designPrinciple: DesignPrincipleType;
    closeGallery: () => void;
    isOpen: boolean;
};

const Gallery: FC<GalleryProps> = ({ designPrinciple, closeGallery, isOpen }) => {
    const [counter, setCounter] = useState(0);

    const galleryLength = designPrinciple.gallery.length;
    const currentGalleryItem = designPrinciple.gallery[counter];

    if (!galleryLength) {
        return null;
    }
    return (
        <Fade in={isOpen} unmountOnExit>
            <Grid
                width='100vw'
                height='100vh'
                position='fixed'
                bgColor='black.100'
                gridTemplateRows='min-content 70vh min-content'
                placeContent='center'
                padding='auto'
                top='0'
                left='0'
            >
                <Flex justifyContent='flex-end' width='100%'>
                    <Fade in={isOpen} unmountOnExit delay={0.3}>
                        <Text cursor='pointer' onClick={closeGallery} size='md'>
                            Close
                        </Text>
                    </Fade>
                </Flex>
                <Image src={currentGalleryItem.url} height='100%' />
                <Grid
                    gridTemplateRows={"min-content min-content"}
                    gridTemplateColumns='auto min-content'
                    gridTemplateAreas={`'title nav' 'desc nav'`}
                    width='100%'
                >
                    <Text gridArea='title' size='md'>
                        {currentGalleryItem.title}
                    </Text>
                    <Text gridArea='desc' size='md'>
                        {currentGalleryItem.description}
                    </Text>
                    <Flex gridArea='nav' alignItems='center'>
                        <LeftArrowIcon
                            boxSize='25px'
                            cursor='pointer'
                            onClick={() => setCounter((curr) => (curr - 1 < 0 ? galleryLength - 1 : curr - 1))}
                        />
                        <Text whiteSpace='nowrap' size='md'>
                            {counter + 1} / {designPrinciple.gallery.length}
                        </Text>
                        <RightArrowIcon boxSize='25px' cursor='pointer' onClick={() => setCounter((curr) => (curr + 1) % galleryLength)} />
                    </Flex>
                </Grid>
            </Grid>
        </Fade>
    );
};

export default Gallery;
