import { Collapse, Fade, Grid, Image, Text } from "@chakra-ui/react";
import { DesignPrincipleType } from "assets/data";
import { FC, ReactNode, useEffect, useState } from "react";
import Gallery from "./Gallery";
import Link from "./Link";

type SectionProps = {
    currentDesign: DesignPrincipleType;
};

const Section: FC<SectionProps> = ({ currentDesign }) => {
    const [content, setContent] = useState<ReactNode>(null);
    const [openGallery, setOpenGallery] = useState(false);
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        const contentParagraphs = currentDesign.content.split("\n");
        const newContent: ReactNode[] = [];
        contentParagraphs.forEach((paragraph) => {
            const contentSplit = paragraph.split(/(?<=<)(.*?)(?=>)/gm);
            const contentParsed = (
                <Text as='p'>
                    {contentSplit.map((text, index) => {
                        if (index % 2) {
                            if (text.includes("link")) {
                                const splittedText = text.split(":");
                                return (
                                    <Link to={splittedText[1]} key={splittedText[2]}>
                                        {splittedText[2]}
                                    </Link>
                                );
                            }
                            return (
                                <Text as='span' variant='humanity' key={text}>
                                    {text}
                                </Text>
                            );
                        } else {
                            return (
                                <Text as='span' key={text}>
                                    {text.replace("<", "").replace(">", "")}
                                </Text>
                            );
                        }
                    })}
                </Text>
            );
            newContent.push(contentParsed, <br />);
        });

        setContent(newContent);
    }, [currentDesign]);

    return (
        <Fade in={!!content}>
            <Grid gridTemplateAreas={`'a a a b b'`} gridTemplateColumns='1fr 1fr 1fr 1fr 1fr' gridGap='15px'>
                {currentDesign && <Gallery designPrinciple={currentDesign} closeGallery={() => setOpenGallery(false)} isOpen={openGallery} />}

                <Grid
                    width='100%'
                    height='100%'
                    gridArea='a'
                    gridTemplateRows='70px 55px auto'
                    opacity={currentDesign ? 1 : 0}
                    transition='opacity 0.5s'
                >
                    <Text
                        as='h2'
                        size='xl'
                        onClick={() => setIsOpen((curr) => !curr)}
                        cursor='pointer'
                        _hover={{ textDecorationColor: "white" }}
                        transition='text-decoration-color 0.5s'
                        textDecor='underline'
                        textDecorationColor='transparent'
                    >
                        {currentDesign?.label}
                    </Text>
                    <Text as='h4' size='xs'>
                        {currentDesign?.tags.sort((a, b) => a.localeCompare(b)).join(", ")}
                    </Text>
                    <Collapse in={isOpen}>{content}</Collapse>
                </Grid>
                <Fade in={isOpen}>
                    <Grid width='100%' height='100%' gridArea='b' paddingTop='195px' gridTemplateColumns='1fr 1fr' gridGap='10px'>
                        {currentDesign?.gallery.map((item) => (
                            <Image
                                src={item.url}
                                width='auto'
                                height='min-content'
                                cursor='pointer'
                                onClick={() => setOpenGallery(true)}
                                key={item.url}
                            />
                        ))}
                    </Grid>
                </Fade>
            </Grid>
        </Fade>
    );
};

export default Section;
