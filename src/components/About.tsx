import { Box, Fade, Grid, Text } from "@chakra-ui/react";
import { FC, ReactNode, useEffect, useState } from "react";

const about = `In this Seminar Work, Aron Dalle Pezze and Boris Bezzola will make an analysis of the first Dark Souls videogame through 37 different <Design Principles>, plus a special one that we called “Mematic Potential”, divided into 13 sections. But first of all, what is Dark Souls?
Dark Souls is an action RPG video game made by FromSoftware in 2011, which became widely known in the following years due to its lore and its infamous difficulty.
In this project we aim to understand the good and the bad designs of Dark Souls which led to its fame.`;

const About: FC = () => {
    const [content, setContent] = useState<ReactNode>(null);

    useEffect(() => {
        const contentParagraphs = about.split("\n");
        const newContent: ReactNode[] = [];
        contentParagraphs.forEach((paragraph) => {
            const contentSplit = paragraph.split(/(?<=<)(.*?)(?=>)/gm);
            const contentParsed = (
                <Text as='p'>
                    {contentSplit.map((text, index) => {
                        if (index % 2) {
                            return (
                                <Text as='span' variant='humanity'>
                                    {text}
                                </Text>
                            );
                        } else {
                            return <Text as='span'>{text.replace("<", "").replace(">", "")}</Text>;
                        }
                    })}
                </Text>
            );
            newContent.push(contentParsed, <br />);
        });

        setContent(newContent);
    }, []);

    return (
        <Box gridArea='content'>
            <Fade in={!!content}>
                <Grid gridTemplateAreas={`'a a a b b'`} gridTemplateColumns='1fr 1fr 1fr 1fr 1fr' gridGap='15px'>
                    <Grid
                        width='100%'
                        height='100%'
                        gridArea='a'
                        paddingTop='65px'
                        gridTemplateRows='70px auto'
                        opacity={content ? 1 : 0}
                        transition='opacity 0.5s'
                    >
                        <Text as='h2' size='xl'>
                            About
                        </Text>
                        {content}
                    </Grid>
                </Grid>
            </Fade>
        </Box>
    );
};

export default About;
