import { Link as ReactRouterLink, LinkProps as ReactRouterLinkProps } from "react-router-dom";
import { Link as ChakraLink, LinkProps as ChakraLinkProps } from "@chakra-ui/react";
import { FC } from "react";

type LinkProps = ChakraLinkProps & ReactRouterLinkProps;

const Link: FC<LinkProps> = (props) => (
    <ChakraLink
        as={ReactRouterLink}
        _focus={{
            outline: "none",
        }}
        {...props}
    />
);

export default Link;
