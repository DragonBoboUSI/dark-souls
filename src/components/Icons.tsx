import { Icon, IconProps } from "@chakra-ui/react";
import { FC } from "react";

export const LeftArrowIcon: FC<IconProps> = (props) => (
    <Icon viewBox='0 0 11 20' color='white.100' {...props}>
        <path d='m10 19-9-9 9-9' stroke='currentColor' />
    </Icon>
);

export const RightArrowIcon: FC<IconProps> = (props) => (
    <Icon viewBox='0 0 11 20' color='white.100' {...props}>
        <path d='m1 1 9 9-9 9' stroke='currentColor' />
    </Icon>
);
