export const designPrinciples = [
    { route: "shaping", label: "Shaping" },
    { route: "entry-point", label: "Entry-point" },
    { route: "consistency", label: "Consistency" },
    { route: "five-hat-racks", label: "Five Hat Racks" },
    { route: "highlighting", label: "Highlighting" },
    { route: "iconic-representation", label: "Iconic Representation" },
    { route: "control", label: "Control" },
    { route: "constraints", label: "Constraints" },
    { route: "wayfinding", label: "Wayfinding" },
    { route: "affordance", label: "Affordance" },
    { route: "forgiveness", label: "Forgiveness" },
    { route: "factor-of-safety", label: "Factor of Safety" },
    { route: "confirmation", label: "Confirmation" },
    { route: "similarity", label: "Similarity" },
    { route: "immersion", label: "Immersion" },
    { route: "hierarchy", label: "Hierarchy" },
    { route: "hicks-law", label: "Hick’s Law" },
    { route: "color", label: "Color" },
    { route: "threat-detection", label: "Threat Detection" },
    { route: "figure-ground-relationship", label: "Figure-Ground Relationship" },
    { route: "symmetry", label: "Symmetry" },
    { route: "self-similarity", label: "Self-Similarity" },
    { route: "prospect-refuge", label: "Prospect-Refuge" },
    { route: "defensible-space", label: "Defensible Space" },
    { route: "performance-load", label: "Performance Load" },
    { route: "face-ism-ratio", label: "Face-ism Ratio" },
    { route: "attractiveness-bias", label: "Attractiveness Bias" },
    { route: "baby-face-bias", label: "Baby-Face Bias" },
    { route: "classical-conditioning", label: "Classical Conditioning" },
    { route: "waist-to-hip-ratio", label: "Waist-to-Hip Ratio" },
    { route: "performance-vs-preference", label: "Performance vs Preference" },
    { route: "cost-benefit", label: "Cost-Benefit" },
    { route: "iteration", label: "Iteration" },
    { route: "80-20-rule", label: "80/20 Rule" },
    { route: "errors", label: "Errors" },
    { route: "ockhams-razor", label: "Ockham’s Razor" },
    { route: "mematic-potential", label: "Mematic Potential" },
];

designPrinciples.sort((a, b) => a.label.localeCompare(b.label));

const designPrincipleLabels = designPrinciples.map((design) => design.label);

export type DesignPrincipleLabel = typeof designPrincipleLabels[number];

export type DesignPrincipleType = {
    route: string;
    label: string;
    tags: DesignPrincipleLabel[];
    content: string;
    gallery: GalleryItemType[];
};

export type GalleryItemType = {
    url: string;
    title: string;
    description: string;
};

const data: DesignPrincipleType[] = [
    {
        route: "beginning-and-tutorial",
        label: "Beginning and Tutorial",
        tags: ["Entry-point", "Shaping"],
        content: `We believe that the myth of Dark Souls’ difficulty is firstly created by its bad <entry-point>. This is due not only by the bad <shaping> done in the tutorial, but also by some more elements like the Menus of the game which will be deepened on their <link:/section/menus:own section>. But let’s go through the beginning and the tutorial of the game step by step.
First of all, while choosing for the starting class of your character you cannot have a clear idea of what its equipment is if not for icons. Then, when you move into personalizing your character, you end up dealing with sliders that are extremely unclear: some of them are connected with each other (so if you move one, others with move at the same time), and each slider is so close to one another that sometimes the player forgets if the labels are above or below the slider and it has to look at the first one to check the pattern.
Since the start of the game, Dark Souls assumes that the player is going to read the description of the items that it will find, since it is extremely important for progressing in certain points, but this isn’t explained in any tutorial message. Even once opening the menu, this is not clearly emphasized enough, as explained in the <link:/section/menus:“Menu” section>.
Then, the player starts moving into the tutorial level of the game, which only gives extremely basic indications on how to play, but doesn’t explain many important and fundamental features such as the types of roll, the scaling of weapons (the fact that the scaling influences the “+x” number of the damage of the weapon is not immediately clear), the poise, the stability of shields and many more things.
Finishing the tutorial, the player is thrown into a world with no clear indication on where to go, and which is the “correct path”. To know more about this, consult the <link:/section/map-and-world:section “Map and World”>. Due to this, the player will end up in places in which it is not really supposed to go, and maybe even get completely stuck. This will give an extremely bad first impression to the player, who might see the game as cruel and unfair and it might lead to the abandonment of the game.`,
        gallery: [
            {
                url: "https://i.ibb.co/0yFzZrw/20211124203204-1-min.jpg",
                description: "Sliders in the character creator",
                title: "Beginning and Tutorial",
            },
        ],
    },
    {
        route: "menus",
        label: "Menus",
        tags: ["Highlighting", "Consistency", "Five Hat Racks", "Iconic Representation"],
        content: `As explained in the <link:/section/beginning-and-tutorial:section “Beginning and Tutorial”>, the player is required to go through the description of the items, and when it opens the menu there are so many information that most people don’t even read them all and miss the “X: Toggle Display”, and even if they read descriptions, those completely lack any kind of <highlighting> of key words. Then, still regarding menus, there are two more big problems: the first one is that there is no <consistency> in one of the buttons (X/Square) in the item menu and in the equipment menu. In the item menu it is used to “Toggle Display”, while in the equipment menu to remove an equipped item, thus bringing people to unequip unintentionally an item when wanting to read its description or stats. The second problem of the menus is the fact that items (just like the order of the areas in the warp menu) are ordered first by category, and then alphabetically by their Japanese name. So, if you play the game in any language that is not Japanese, the ordering makes no sense at all and it doesn’t really respect the <Five Hat Racks> design principle, causing players to waste time looking for the correct item.
Also, Dark Souls has a good <icon representation> for the character’s stats that are symbolic icons, but they are just acceptable for some status effects (such as blood loss). In any case, they are a huge improvements when compared to the icons of its predecessor, Demon’s Souls, which were completely not intelligible.`,
        gallery: [
            {
                url: "https://i.ibb.co/6mrSWBP/20211124203623-1-min.jpg",
                description: "Ordering of items",
                title: "Menus",
            },
        ],
    },
    {
        route: "map-and-world",
        label: "Map and World",
        tags: ["Control", "Constraints", "Wayfinding"],
        content: `Completing the tutorial, the player reaches the true world of Dark Souls, finding itself in Firelink Shrine.
From here, the player has complete <control> on where to go. From this point onward, the game has two ways of applying <constraints>: locked doors and orange fogs. The map of the game can then be divided as follows: introduction, first half, middle part, second half and epilogue (without considering the DLC). To proceed from one section to the other, the player must first satisfy certain requirements, which are the <constraints>. Between the introduction and the first half, you have a locked door. Between the first half and the second half, you have the orange fogs. And between the second half and the epilogue, you have another locked door. The middle part is an in-between which is locked by a door and which then removes the orange fogs.
In the Map here on the right we have in yellowish the introduction, in black and white the first half, in purple the middle part, in red the second half and in lime green the epilogue.
Right when the player arrives in Firelink Shrine, it has three different paths that can be taken, but only one of them is really feasible for a new player. So, the <control> given to the player is really good for an expert and it can be even increased through the Master Key, but it is pretty bad for a new player, who can end up in a place where it is not supposed to go with its current skill. This could even have been fixed by blocking the Catacombs and New Londo with the Master Key as well (and fun fact, the elevator to New Londo should have been closed with the Master Key, but during development that door has been moved to the Drake Valley instead).
Also, even though Dark Souls doesn’t give a consultable map to the player and its world in pretty big and interconnected, all areas are connected to one another in a 1:1 scale without strange elevators on on top of towers that bring you to a lava world that looks like a Super Mario final level (we’re looking at you, Dark Souls II). Thanks to this feature, the game gives a possibility of <wayfinding> (orienting) through looking at the environment, up to the point that many players don’t even know how many locations of the game you can see from some specific places. For example, you can see The Great Swamp from Firelink Shrine, and from Darkroot Garden you can see both the bridge of the Moonlight Butterfly boss fight and the bridge of the wyvern that connects the Undead Burg to the Undead Parish.`,
        gallery: [
            {
                url: "https://i.ibb.co/9T5bBMQ/DkS-min.png",
                description: "Map of all the areas (DLC excluded)",
                title: "Map and World",
            },
            {
                url: "https://i.ibb.co/5jt8Zbz/Blighttown-and-Great-Swamp-min.jpg",
                description: "The Great Swamp from Firelink Shrine",
                title: "Map and World",
            },
            {
                url: "https://i.ibb.co/HDRh4jk/Moonlight-Butterfly-min.jpg",
                description: "The Moonlight Butterfly from the Darkroot Garden",
                title: "Map and World",
            },
            {
                url: "https://i.ibb.co/pWSj70s/Tower-and-Bridge-min.jpg",
                description: "The Moonlight Butterfly from the Darkroot Garden",
                title: "Map and World",
            },
        ],
    },
    {
        route: "to-tell-or-not-to-tell",
        label: "To Tell or Not To Tell?",
        tags: ["Affordance"],
        content: `Dark Souls’ mechanics and maps have been deeply reasoned by the designers, as suggested by some interviews with them. The objective of the director of the game, Hidetaka Miyazaki, was that of making designs “not immediately understandable” but also intelligible. As he also says, that is a balance that is really hard to get, and sometimes a design can unintentionally fall into one extreme or the other. So, in which cases in Dark Souls such balance has been achieved and when it has not?
We can start by two specific cases of voluntarily “not telling too much” which ended up being considerable hidden <affordance>. The first one is the walkable buttress of Anor Londo, which is the first one in which the players to walk on to proceed in the game is visually exactly the same as others which would kill the player if it tried to walk on them. But this is not the case for the following walkable buttresses, which are different due to added visual elements such as some sort of “handrails”. The second one are the ladders in blighttown, which are completely invisible. The designers tried to add some torches to signal the presence of ladders, but this has not been understanded by the players, who just thought that those were there to add lighting to an area which is pretty dark.
Then, we can take two other examples of hidden <affordance> which are in these cases due straight up to <errors>. The first one is the fact that Lloyd’s Talisman can be used on Mimics to make them asleep, which is not written or signalized anywhere. The second one is the bad geodatas of the Great Hollow, which don't make clear at all which places are walkable and which are not.
There are still multiple minor cases of hidden <affordance>, like the types of roll or other mechanics cited here and there in other sections. But on the other hand, Dark Souls also respects the <affordance> principle multiple times through input prompting and many trivial examples such as levers or doors.`,
        gallery: [
            {
                url: "https://i.ibb.co/Khz56WV/Anor-Londo-Arc-3-min.jpg",
                description: "Walking on a buttress in Anor Londo",
                title: "To Tell or Not To Tell?",
            },
            {
                url: "https://i.ibb.co/ZxjhMQq/Blighttown-min.jpg",
                description: "Torches for the stairs in Blighttown",
                title: "To Tell or Not To Tell?",
            },
            {
                url: "https://i.ibb.co/W046Yyv/affordance-waiting-min.jpg",
                description: "Waiting in the nest of the crow",
                title: "To Tell or Not To Tell?",
            },
        ],
    },
    {
        route: "difficulty",
        label: "Difficulty",
        tags: ["Forgiveness", "Factor of Safety", "Confirmation", "Consistency", "Similarity", "Immersion"],
        content: `Other than the problems deepened in the <link:/section/beginning-and-tutorial:“Beginning and Tutorial” section>, one of the biggest myths of Dark Souls’ difficulty is the fact that it is merciless, which means that it doesn't respect the <forgiveness> design principle at all. This can be seen by how much harsh it is in the entire area of Blighttown or The Great Hollow, but also with unfair boss fights such as the Capra Demon (which has a far too small arena) or the Bed of Chaos (in which the floor can fall under your feet with no possibilities for you to do anything if you destroy the things at the sides of the bossfight using long range weapons such a bow).
All this though is not really correct, since Dark Souls is in fact also sometimes very forgiving. For example, geodatas are extremely wider than what they seem to be to avoid your fall (highly noticeable both in Anor Londo and in Sen’s Fortress), and it allows you to retrieve your souls when you die instead of directly making you lose any progress that you had made.
Dark Souls has also been thought to have a <factor of safety> for what concerns bugs. Bugs shouldn’t happen in theory, but in fact we all know that it is impossible to avoid them completely. For this reason, the developers introduced two big factors of safety: the first is an opened chest in Firelink Shrine, of which many players don’t understand the function. Said chest has the scope of containing key objects that the player might have lost by dropping them (which was possible only in the pre-patched version) or due to bugs. The second big factor of safety is the possibility to return to safe places in case that the player ends up somewhere out-of-bound without the death triggering or more in general if it gets stuck somewhere. This is achieved mainly by moving the player to the last safe location if it exits and re-enters the game, and if that doesn’t work, then there is an undroppable item which can be used to return to the last checkpoint (the dark sign).
Another help to the players comes in the form of a so-called “quality of life mechanic”, which in this case is simply achieved through the addition of the <confirmation> design principle. A <confirmation> is always asked to the player whenever it wants to upgrade a weapon, level up, use certain items and so on.
But going back, before in this same section we also discussed “unfair boss fights”. Although the two previously mentioned boss fights are pretty unfair, the game also features elements to increase the <immersion> of the player, and somehow “calibrate” the difficulty of a fight. This is achieved through the possibility of farming/grinding in order to power up your equipment or, more importantly, to level up your character a lot, thus widely reducing the difficulty of a boss fight. Also, there is the possibility to summon NPC or other players to receive help against a certain enemy. Even though these elements can lead people to make the game incredibly easier for them, they are also extremely important tools to adjust the difficulty to the ability of a player, since not everyone has the same level of competences.
Also, enemies feature an element of <similarity>: similar enemies will have similar attacks. This is clearly visible by taking into the account the boss fights of the Stray Demon and of the Demon Firesage. This is further enhanced by a <consistency> element, like the weakness of specific enemies or the fact that a mimic is distinguishable from a normal chest by the form of its chain.`,
        gallery: [
            {
                url: "https://i.ibb.co/JQrtJY2/geodata-min.jpg",
                description: "Geodata in Anor Londo",
                title: "Difficulty",
            },
            {
                url: "https://i.ibb.co/0nr3fMd/20211124204010-1-min.jpg",
                description: "Opened chest in Firelink Shrine",
                title: "Difficulty",
            },
            {
                url: "https://i.ibb.co/dQrt2x1/confirm-min.jpg",
                description: "Confirmation screen",
                title: "Difficulty",
            },
            {
                url: "https://i.ibb.co/9TfDZFx/Similarity-1-min.jpg",
                description: "Stray Demon",
                title: "Difficulty",
            },
            {
                url: "https://i.ibb.co/3BQHL3g/Similarity-2-min.jpg",
                description: "Demon Firesage",
                title: "Difficulty",
            },
            {
                url: "https://i.ibb.co/Cb3N8mW/mimic-min.jpg",
                description: "Chain of mimics",
                title: "Difficulty",
            },
        ],
    },
    {
        route: "weapons",
        label: "Weapons",
        tags: ["Hierarchy", "Hick’s Law"],
        content: `Dark Souls features many different weapons divided into categories, each with its own moveset. Even though there are multiple movesets, many of them have similarities in specific attacks, and each weapon deep down ends up basically having four types of really different attacks (one-handed and two-handed light and heavy attack), thus following <Hick’s Law>.
But when you have to deal with weapons, a really important element is their upgrade. They can not only be simply upgraded, but they can also be turned into Boss weapons or “infused” weapons (this term actually comes from Dark Souls III, since in the original Dark Souls they were simply called “upgrades”, causing confusion if compared with the “regular upgrade”). Even so, the path to reach each type of weapon evolution is highly unclear; for example, to make an electric infusion you need a +10 weapon, while for a fire infusion a +5 weapon is enough. A <hierarchy> should be made clearly visible to the players, as happens for example in Monster Hunter World. In fact, in the Dark Souls community a <hierarchy> image is often used to explain infused weapons.`,
        gallery: [
            {
                url: "https://i.ibb.co/k605WzP/upgrade-min.jpg",
                description: "Weapon evolution hierarchy",
                title: "Weapons",
            },
        ],
    },
    {
        route: "colors-of-areas-and-enemies",
        label: "Colors of Areas and Enemies",
        tags: ["Color", "Threat Detection", "Figure-Ground Relationship"],
        content: `We started the analysis on the graphical level of Dark Souls by checking the <color> schemes of all the main areas of the game (with only a few missing). We divided them by the type of <color> schema used and then again by the specific <colors> used. The result has been that all Dark Souls’ areas follow either the Analogous or the Monochromatic <color> schemes. Of them the most common analogous <colors> are in the range of blue and green (10), with only a few red and yellow (3), while the most common monochromatic scheme is the black and white/ grayscale (5). So, roughly 50% of the areas have a <color> palette that is in the blue and green range, ~25% on the black and white/grayscale, ~15% on the red and yellow range and ~10% has a monochromatic <color> palette that is not simply the black and white/grayscale, but most of them have cold <colors>. So, the majority of Dark Souls’ areas have a cold and dark <color> palette.
It is also interesting to notice that the areas of all the Dark Souls that the players find most beautiful are of two opposite <color> temperatures. One is Anor Londo, with a yellow-to-red analogous schema, while the other one is Irithyll from Dark Souls III, which has monochromatic light blue schema. Also, those two cited areas are also between the most hot and cold areas respectively, if not for the areas full of lava (like Lost Izalith or the Iron Keep from Dark Souls II) which are though usually disliked.
Following the analysis of the areas, we moved into checking the enemies inside the different areas. Do they share the same <color> palette of the environment they’re in, or they don’t? And if they don’t, do they enhance the schema or do they create a bad <color> combination? To answer this, we ended up dividing enemies into five categories: enemies that mimetize in the environment, enemies that share the same palette of the environment, enemies that have a small contrast with the environment, enemies with a high contrast with the environment and special cases. So, none of them are actually bad if compared to the area they’re in. Also, there are some missing enemies; this is due to the fact that if for areas we personally took the screenshots ourselves, for enemies it would have been a far too big task, so we only included the enemies of which we found an image in the wikia of Dark Souls. Other than that, we also ignored different types of the same kind of enemy which are in the same exact area (like the different undeads of the Undead Burg).
Before going more in depth with the categories (except the second one, which is pretty self-explanatory), I must explain why there are equal enemies in multiple categories. This is due to the fact that those enemies appear in different areas with different <color> palettes, thus blending in differently depending on the area. Those enemies are specifically the Moonlight Butterfly, the Crystal Golems and the Mushrooms. The first two appear both in the Darkroot Garden/Basin (which has a green-blue <color> palette) and in the Crystal Cave (which has a blue monochromatic palette), while the mushrooms appear both in The Great Hollow (monochromatic brown) and in the Darkroot Garden (green palette).
The enemies of the first category are sometimes well thought in order to be hidden and remove from the player the <threat detection> possibility (like all the enemies from the Darkroot Garden placed in this category), but some other times they are just so poorly designed that they become really hard to distinguish from the background even when they are not supposed to be hidden (all the enemies from the Demon Ruins and from Lost Izalith in this category), thus not following the <figure-ground relationship> design principle.
The enemies of the third category are placed there based on the fact that they somehow create a contrast with the environment. This is can be due to multiple reasons, which are: being of a brighter <color> than the environment (like the Sanctuary Guardian, the Drakes or the Flaming Dogs), having a single element that creates a small visible contrast (like the blue drapes of Artorias or the orange mantle of the Balder knights) or having an element that creates a strong contrast with the environment (like the eyes of Kalameet, Manus and the Bloathead Combats, the strong white aura around the Humanities or the light blue light of Sif’s sword). It is also interesting to notice how some of these enemies use their contrasting <color> to signal their next attack to the player, like Sif’s sword that becomes brighter before certain attacks or Kalameet’s eye that shines when he’s going to do his grab move.
The enemies of the fourth category are very few, but all pretty interesting. The drake on the bridge is of a red <color> on a dark green-yellow area, and there is not much to add to it. The next two cases are much more interesting: the Engorge Hollows and Queelag add a red <color> to a area that is basically monochromatic on a black and white/grayscale, while the frog and the mushroom are both red creature in a mostly green area, thus creating a complementary <color> scheme.
Finally, the last category is the one in which every single enemy placed in it has to be discussed separately. First of all, we have the Four Kings: they are light gray/white enemies in an area that is completely black. They follow the monochromacy of the area, but they create a huge contrast since they are the only non-black element in the whole environment (aside from the player, obviously). Then, the Demon Firesage is an enemy that completely follows the red <color> palette of the general area in which it is, but the specific arena in which it is fought doesn’t have that same <color> palette and it’s instead more generally gray. In the same style there is Gwyn, which follows the gray/extremely desaturated <color> palette of the area he’s in, but his flaming sword is extremely visible due to its bright orange light. The fact that the arenas of both the Demon Firesage and Gwyn are grayish is something that many people don’t notice immediately, and it’s exactly the reason for which they are both in this category. This is due to the fact that the orange light that they produce is so strong to give the same <color> to the whole neutral-<color>ed arena they’re in, up to the point that the whole area seems to be orange like them. Lastly, Gwyndolin is another very interesting case to analyse. Its design is composed of some golden elements, a white robe and some snakes which are of a brown and desaturated green <color>. What is really interesting, is that Gwyndolin’s arena can either have a warm <color> palette (if fought in the normal Anor Londo) or a really cold one (if fought in the Dark Anor Londo). In the normal Anor Londo, Gwyndolin’s ornaments are perfectly in line with the environment <colors>, but the snakes do not, but the opposite happens in the Dark Anor Londo, while the neutral <color> of the robe doesn’t stand up in either situation. So, in one case there is a contrast with the environment due to the ornaments (dark location), while in the other case due to the snakes (normal location).
To conclude the discussion on the <color> schema of areas and enemies, we decided not to take into account enemies that appear in many different locations (such as the Black Knights) nor the NPCs (for the same reason), except for some special case (Gwynevere) that appears on her <link:/section/gwynevere:own section>.`,
        gallery: [
            {
                url: "https://i.ibb.co/bFC7nHP/Areasfinal-min.jpg",
                description: "Color schemas of Areas",
                title: "Colors of Areas and Enemies",
            },
            {
                url: "https://i.ibb.co/V2tqmGB/collage-nemici-min.jpg",
                description: "Colors of Enemies",
                title: "Colors of Areas and Enemies",
            },
        ],
    },
    {
        route: "development-of-the-areas",
        label: "Development of the Areas",
        tags: ["Color", "Symmetry", "Self-Similarity", "Prospect-Refuge", "Defensible Space", "Performance Load", "Threat Detection"],
        content: `While looking for some intel from the developers of the game for better understanding the development history of Dark Souls, we ended up finding a really interesting interview with the director of the game, Hidetaka Miyazaki, and most of the designers. Aside from many information that are irrelevant to the scope of this analysis, we found out that some areas are inspired by real-life locations. Said locations are New Londo, the interns of the Undead Parish and Anor Londo.
Starting from New Londo, it was based on Mont-Saint-Michel, in France. As said by one of the designers, “all but the finest details were already in place”, and it has thus been one of the locations of the game that required the least amount of work as far the design is concerned.
Then, Anor Londo was based on multiple real life cathedrals and castles, and for example the double helix staircase is based on the one that exists in the Château de Chambord, France.
Since all three areas are based on real-life castles and cathedrals, it is obvious that they all present multiple elements of <symmetry> and <self-similarity>, as clearly visible in the images of Anor Londo and of the Undead Parish here on the right. <Symmetric> elements appear also in more locations, like the wyvern bridge, The Depths or the Duke’s Archive, which latter is by the way inspired by Harry Potter, having even movable staircases.
In multiple instances of interviews or comments of the developers, it came up the fact that the Demon’s Ruins and Lost Izalith have been rushed areas that didn’t really go through a good quality check. Those two areas weren’t supervisioned by the director of the game and some parts of them have even been made by external studios (like for example the 3D model of the Bed of Chaos). We can in fact notice that those are between the worst areas of the first Dark Souls, and even their <color> palette is pretty strange for the game. If we refer to the analysis made on the <link:/section/colors-of-areas-and-enemies:“Colors of Areas and Enemies” section>, we can see that the Demon’s Ruins and Lost Izalith are two of the only three areas in the game with a warm <color> palette. Even so, there is something strange about them. If we take Anor Londo as comparison, said area is really “warm”, meaning that it even gives a feeling of calming warmth. Anor Londo feels like a peaceful area, but on the other hand the Demon’s Ruins and Lost Izalith have an extremely aggressive <color> palette, which is in total contrast with the whole rest of the game. And not only that, their <colors> and brightness make them extremely chaotic, up to the point of being able to <detect threats> or even to be unable to see items on the ground due to a chaos and a brightness so elevated that causes a sort of bad <performance load>. In other words, it requires so much effort to even only look carefully around the map to search for the white light of items on the ground that almost every player won’t even try to do it.
One last area that is really interesting to discuss is Firelink Shrine, the “central hub” of the game. In the original concept, Firelink Shrine should have been a flourishing area, with lively <colors>, plants (perhaps even flowers) and even little rivers. It should have felt like a heaven on earth, a place that is cared for and in which you can feel safe, like a <defensible space>. Based on this idea, the designers also decided to give its specific location which was kept even in the final version of the game, which is that of a place from which you can see multiple places of the game (like the Catacombs, the Great Swamp and the Undead Burg). But the idea of a flourishing <defensible space> was then cast aside when they realized that it would have been too much in contrast with the design of all other areas. After a few different attempts, they ended up with the final and current version of Firelink Shrine, which is an area with a <color> palette in line with the rest of the game, but they decided to keep somehow the idea of a place where all the NPCs could stay safely. This has been achieved by creating a structure with multiple walls and almost hidden location, so that every NPC could go in a place where it could feel safe and covered from other characters while still having the possibility to observe their surroundings, thus creating some kind of location that recalls the prospect-refuge design principle.
A really important thing that came up while working on the design of Firelink Shrine has been the idea of the bonfires, not meant here as the “checkpoint” mechanic but more on a design level. As said in the <link:/section/colors-of-areas-and-enemies:“Colors of Areas and Enemies” section>, most of Dark Souls’ areas are cold areas. Therefore, a calming warmth as the one described previously while comparing Anor Londo with the two other warm areas, would be the perfect fit for making the players feel safe in a cold world. This way, the bonfires feels even more safe thanks to the temperature contrast with the area they’re in, and it can be noted that even though they feel safe in general since they’re checkpoints, they actually feel less safe and calming in the in three previously-mentioned areas (Anor Londo, the Demon’s Ruins and Lost Izalith).`,
        gallery: [
            {
                url: "https://i.ibb.co/0CXMhXr/Anor-Londo-min.jpg",
                description: "Anor Londo",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/tMzKBDc/20211122172810-1-min.jpg",
                description: "Arena of Gwyndolin in Anor Londo",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/ChFXydM/Undead-Parish-min.jpg",
                description: "Inner Undead Parish",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/ZJyF0Xf/Drake-Bridge-min.jpg",
                description: "Bridge of the Wyvern",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/vLbcBVM/Depths-min.jpg",
                description: "The Depths",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/02dF1ww/The-Duke-s-Archives-min.jpg",
                description: "Duke Archives",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/bgyWnJS/Culi-di-Drago-min.jpg",
                description: "Lost Izalith",
                title: "Development of the Areas",
            },
            {
                url: "https://i.ibb.co/x6wJV0n/Firelink-Shrine-min.jpg",
                description: "Firelink Shrine",
                title: "Development of the Areas",
            },
        ],
    },
    {
        route: "gwynevere",
        label: "Gwynevere",
        tags: ["Face-ism Ratio", "Attractiveness Bias", "Color", "Baby-Face Bias", "Classical Conditioning", "Waist-to-Hip Ratio"],
        content: `Gwynevere is one of the most important characters in Dark Souls when it comes to analyzing design principles. Quoting Hidetaka Miyazaki, the director of the game, “A giant, considerate, caring woman. The kind we all lost when we grew up; that's what I wanted to make.” But how did they manage to achieve such a goal?
Before moving into discussing it more in depth, it is imperative to briefly discuss her “<waist-to-hip>” ratio, which is a gentle way to say that she has massive breasts. This element of Gwynevere is probably one of the most famous Dark Souls’ elements for the players, and it even gave birth to many <memes> about that, as it is discussed in the <link:/section/meme:section “Meme”>. Even so, as Hidetaka Miayazaki tells us, this was not intentional: “[...] her breasts have nothing to do with me, they happened without my knowledge. It's all the artist's fault. [...] the artist had such a happy look on his face, I didn't have the heart to stop him.”
Going back to the main topic, we can start by the fact that the warm <color> palette of the chamber she’s in is the first step into creating the aura of the “caring woman” that the director wanted to make. Then, Gwynevere features extremely soft physical features, with a visage that incorporates the <baby-face bias> design principle. Her dimensions also help in seeing her as a mother in a relationship in which the player is still a small child, and this together with her <face-ism ratio> helps in trusting her much more, and to not feel on your guard in her presence. But if Dark Souls is a video game in which the player can freely move the camera, how can we talk about <face-ism ratio>? This is possible not only through the fact that the player can only see her full body unless it uses a binocular, but also through the fact that Gwynevere is depicted full body also in a painting inside one of the chamber rooms of the main palace of Anor Londo.
But why did we specifically say that it “helps in trusting her”? That is important if we analyze the character also from a story point of view (with needed spoilers ahead).
In Dark Souls, Gwynevere (last warning: spoilers ahead) is actually an illusion created by her sister/brother Gwyndolin. Gwyndolin does that because (s)he knows that (s)he’s ugly and no one would ever trust her/him. Acknowledging the <attractiveness bias>, Gwyndolin created the illusion of his/her sister to use as proxy to make the characters inside the game (and the player) do want (s)he wants. This can also be connected, even if it’s a bit far-fetched, to the <classical conditioning> design principle, through which the players think that following the directions of such a beautiful and warm woman must mean being on the morally right path inside the game’s story. `,
        gallery: [
            {
                url: "https://i.ibb.co/ZMTVZKd/Gwynevere-min.jpg",
                description: "Gwynevere",
                title: "Gwynevere",
            },
            {
                url: "https://i.ibb.co/qkdx85q/20211124203544-1-min.jpg",
                description: "Portrait of Gwynevere in a chamber in Anor Lond",
                title: "Gwynevere",
            },
            {
                url: "https://i.ibb.co/WzS17FR/Gwyndolin-min.jpg",
                description: "Gwyndolin",
                title: "Gwynevere",
            },
        ],
    },
    {
        route: "priscilla",
        label: "Priscilla",
        tags: ["Affordance", "Baby-Face Bias", "Color", "Mematic Potential"],
        content: `Priscilla is a character which could have been cited in five different other sections: <link:/section/to-tell-or-not-to-tell:“To Tell or Not To Tell?”>, <link:/section/colors-of-areas-and-enemies:“Colors of Areas and Enemies”>, <link:/section/menus:development-of-the-areas“Development of the Areas”>, <link:/section/gwynevere:“Gwynevere”> and <link:/section/meme:“Meme”>. But in any of those, she would have had just half a line with her name, or it would have directly been hard to make her fit in, so we decided to create a section specifically for her, but which relies on the content of the other previously-mentioned five sections.
Priscilla was firstly designed to be of the same size as the player’s character and based on what the authors said about her in some interview, she should have been the “heroine” of Dark Souls, but what they meant exactly by that has never been deepened. We can only imagine that she wouldn’t have been the playable character since in an interview they said that she should have relocated to Firelink Shrine at a certain (unknown) point in the game. In any case, her character was discarded during development due to the fact that her design wasn’t fitting the general style of the game any longer. But when they found the way to insert the area of the Painted World of Ariamis, which was at the beginning just a test area and not an actual location of the game, they decided to put her there due to the fact that her <color> schema was perfectly in line with that of that area.
Moving to the character itself, she shares the facial features of Gwynevere with the <baby-face bias> design principle, but in this case she does specifically for the reason of emanating innocence, as her dialogues stress (even though we know by playing that the fact that the inhabitants of Ariamis are kind is a filthy lie, which became a <meme>). But not much more can be said about here, since even her own story and lore is quite a mess since she’s been added lately in development and she ended up inheriting also part of the lore of another character, Beatrice, which should have been an NPC with her own quest (but she ended up being cut-content, appearing also as a summonable phantom). In fact, the doll that is used to enter Ariamis and that people tie to Priscilla should have originally been tied to Beatrice.
Then, it is interesting to notice a good example of <affordance> in her boss fight. Right at the beginning of the fight (and again sometimes during it), Priscilla will become invisible, and her positions will be understandable by looking at her footprints on the snow on the ground of her arena.
Lastly, she contributes to the <mematic potential> of Dark Souls through her bare feets and her “fluffy tail”, which has been incredibly loved by the fans of the game.`,
        gallery: [
            {
                url: "https://i.ibb.co/7CvHCZD/Priscilla-min.jpg",
                description: "Priscilla",
                title: "Priscilla",
            },
            {
                url: "https://i.ibb.co/BgGGBdx/artworks-000413832228-f1lgvo-t500x500.jpg",
                description: "Peaceful land",
                title: "Priscilla",
            },
            {
                url: "https://i.ibb.co/pRyn4WW/affordance-priscilla-min.jpg",
                description: "Footprints on snow",
                title: "Priscilla",
            },
        ],
    },
    {
        route: "beauty-in-monstrosity",
        label: "Beauty in Monstrosity",
        tags: ["Waist-to-Hip Ratio"],
        content: `One of the most famous Dark Souls characters is without a doubt Queelag, the beautiful half-spider half-woman boss. It is easy to associate the <waist-to-hip> ratio design principle to her, but it is obvious that her fame also comes from the fact that she’s basically naked.
It is in fact much harder to talk about “beauty” in the monsters of Dark Souls starting from the <waist-to-hip> ratio with creatures such as Seath the Scaleless and the Gaping Dragon. It is clear that the <waist-to-hip> ratio is not the (only) reason for which those two look “cool”, but it is also due to the deep carefulness that has been put into their design so that they would look like not just monsters, but also as fallen noble creatures. Their design is not created in order for them to be disgusting, but for them to look like ancient noble beings that have fallen into being deformed monsters.`,
        gallery: [
            {
                url: "https://i.ibb.co/8P1b6Q0/Queelag-min.jpg",
                description: "Queelag",
                title: "Beauty in Monstrosity",
            },
            {
                url: "https://i.ibb.co/82ZYX6Z/Seath-the-Scaleless-min.jpg",
                description: "Seath the Scaleless",
                title: "Beauty in Monstrosity",
            },
            {
                url: "https://i.ibb.co/KjWtbsC/Gaping-Dragon-min.jpg",
                description: "Gaping Dragon",
                title: "Beauty in Monstrosity",
            },
        ],
    },
    {
        route: "meta",
        label: "Meta",
        tags: ["Performance vs Preference", "Cost-Benefit", "Iteration", "80/20 Rule", "Errors", "Mematic Potential"],
        content: `When someone plays almost any game, it is required to make certain choices. The <cost-benefit> design principle is visible in almost every video game, and this goes for Dark Souls as well. “Should I take this fall damage in order to cut through part of the area?”, “Should I make an extra attack even if that would mean being hit as well?”, “Should I kick Lautrec off the cliff in order to get his ring?”. These are just a few of the many examples that could be made to explain the <cost-benefit> choices that the players are continuously required to make while playing the game, many of which are just so trivial that it is unnecessary to specify them.
On the other hand, what is interesting is talking about the <performance vs preference> design principle. It appears in many video games, but here in Dark Souls appears also in a specific and really important way for the community. It is the so-called “Fashion > Stats”. The game features many different equipment for the characters that are visible up to the point that the community initially split up between people who preferred to have a character that looks cool and those who preferred to have a character that is as efficient as possible. During time, those who looked only at statistics ended up as being seen as “weak players” that needed good stats to beat the game, while those who focused on the fashion were those who could beat the game no matter the stats of the character. This brought the community to use the formula “Fashion > Stats” and even to call the game “Fashion Souls” instead of “Dark Souls”.
But moving now more onto the development side, we can talk about <iteration> with a specific example, not only based on the first Dark Souls itself, but on the whole “Souls series”. In Demon’s Souls, you end up in a swamp that is poisonous. Then, in Dark Souls the same thing happens in the Great Swamp, and later on also in an area of Lost Izalith. In the following game, Dark Souls II, the poisonous swamp appears again, and even twice. Finally, in Dark Souls 3, three swamps appear, and the player already knows that they are poisonous. They repeated the pattern so many times that now all the Souls players already know that swamp = poison. But the true question is: are you really happy with only those poisonous swamps? You shouldn’t be, because Elden Ring is right around the corner!
Going even more on the development side, we can even talk about the <80/20 rule> and <errors>. The biggest part of the game has been achieved without too much effort, but then the last stretch has been dramatic, and the developers ended up having to cut content and to rush certain areas (like Lost Izalith, as explained in the <link:/section/development-of-the-areas:section “Development of the Areas”>). This also lead to certain <errors>, the most visible of all is the position of Artorias’ grave, which in the game is in a different location (the one in which the player fights Sif) than the one seen in the DLC (which is where the player fights Artorias).
Lastly, there is the important concept of the <mematic potential>, and how the developers did or did not think about inserting the potential for elements to become viral, but this topic is addressed especially in its own specific section: <link:/section/meme:“Meme”>.`,
        gallery: [
            {
                url: "https://i.ibb.co/pxmwQ9n/Poison-Swamp-3-min.jpg",
                description: "Poisonous Swamp in Lost Izalith",
                title: "Meta",
            },
            {
                url: "https://i.ibb.co/bbxvjbQ/swamp-min.jpg",
                description: "Elden Ring will feature poisonous swamps",
                title: "Meta",
            },
            {
                url: "https://i.ibb.co/rxZDQ6X/image-2021-11-22-14-44-34-min.png",
                description: "Different location between the grave and the arena of Artorias",
                title: "Meta",
            },
        ],
    },
    {
        route: "meme",
        label: "Meme",
        tags: ["Errors", "Ockham’s Razor", "Mematic Potential"],
        content: `We define the “<mematic potential>” as the potential and the power of a game to have elements that will become viral on the web. Sometimes they happen by mere coincidence, like the “Praise the Sun” of Dark Souls, while sometimes they happen by choice, but this is never the case for Dark Souls. For example, we can take into account an evident <error> of FromSoftware which brought the web to create many <memes>: the entire existence of Dark Souls II. And in fact, related to that game, we can also say that a design principle has been shattered: <Ockham’s Razor>. In fact, what is useless should be removed, so we advise you to just skip Dark Souls II if you want to play the Souls series.
Jokes aside, we can go back to “Praise the Sun”. Why did we say that it wasn’t intentional? First of all, this has been confirmed by Dark Souls’ director, Mizayaki, himself, who said that he never expected that phrase to become so viral. But why was it so spammed inside the game, then? Wait… spammed?
“Praise the Sun” actually appears only once as a line inside the game: in the name of the gesture used by Solaire. But why does every player remember to have read that line many times? This is thanks to something with a huge <meme> potential that FromSoftware didn’t recognize at all during development, and it’s the mechanic of being able to write messages for all players to see by using only certain combinations of pre-made words.
This stimulated the fantasy of the players, who tried to achieve better and better ways to communicate what they wanted, and one of the most simple lines that the game already gives you is in fact “Praise the Sun!”. But then, players started to use that mechanic to create messages with a double meaning, which also became extremely viral. The two most renowned ones are “Try tongue but hole” and “Amazing chest ahead”, latter of which can always be found in front of Gwynevere’s chamber.
With the growth of the community, Dark Souls’ <memes> escalate to the point that it is almost hard to track them all and it would be almost a project on its own to document them all. And sometimes, those <memes> might even backlash someone, as happened with the <memes> of the “bare footed ladies” of the Souls when the players saw that the new Souls lady that appears in Elden Ring has shoes.`,
        gallery: [
            {
                url: "https://i.ibb.co/QF49FfK/feet-min.png",
                description: "Melina has shoes",
                title: "Meme",
            },
        ],
    },
];

export const sections = data.map((item) => ({ route: item.route, label: item.label }));

export default data;
