import { ChakraProvider } from "@chakra-ui/react";
import { Global } from "@emotion/react";
import { HashRouter, Routes, Route } from "react-router-dom";
import { defaultTheme } from "./theme";
import Root from "./views/Root";

const Fonts = () => (
    <Global
        styles={`
      @font-face {
        font-family: "Big Caslon FB Bold";
        font-style: normal;
        font-weight: 700;
        font-display: swap;
        src: url('./fonts/bigcaslon-webfont.woff') format('woff');
      }
      /* latin */
      @font-face {
        font-family: "Garamond Book";
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url('./fonts/ITCGaramondStd-Bk.woff') format('woff');
      }
      `}
    />
);

export const App = () => (
    <ChakraProvider theme={defaultTheme}>
        <Fonts />
        <HashRouter basename='/'>
            <Routes>
                <Route path='/*' element={<Root />} />
            </Routes>
        </HashRouter>
    </ChakraProvider>
);
