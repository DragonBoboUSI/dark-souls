import { extendTheme, ThemeComponents } from "@chakra-ui/react";

const colors = {
    black: {
        100: "#000000",
    },
    white: {
        100: "#FFFFFF",
    },
};

const components: ThemeComponents = {
    Heading: {
        baseStyle: {
            color: "white.100",
        },
        sizes: {
            sm: { fontSize: "12pt" },
            md: { fontSize: "15pt" },
            lg: { fontSize: "21pt" },
            xl: { fontSize: "40pt" },
            "2xl": { fontSize: "60pt" },
        },
    },
    Text: {
        baseStyle: {
            color: "white.100",
        },
        variants: {
            humanity: {
                color: "black.100",
                textShadow: "0px 0px 4px rgba(255, 255, 255, 0.4), 0px 0px 4px rgba(255, 255, 255, 0.8)",
            },
        },
        sizes: {
            sm: { fontSize: "12pt" },
            md: { fontSize: "15pt" },
            lg: { fontSize: "21pt" },
            xl: { fontSize: "40pt" },
        },
        defaultProps: {
            size: "lg",
        },
    },
    Link: {
        baseStyle: {
            color: "white.100",
            borderBottom: "1px solid",
            borderBottomColor: "white.100",
            outline: "none",
            _hover: {
                textDecor: "none",
                color: "white.100",
            },
            _focus: {
                outline: "none",
            },
        },
        defaultProps: {
            size: "md",
        },
    },
};

const fonts = {
    heading: "Big Caslon FB Bold",
    body: "Garamond Book",
};

export const defaultTheme = extendTheme({ colors, fonts, components });
