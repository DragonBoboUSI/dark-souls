import { Box } from "@chakra-ui/react";
import { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import data, { DesignPrincipleLabel, designPrinciples } from "assets/data";
import { DesignPrincipleType } from "assets/data";
import Section from "components/Section";

const DesignPrinciple: FC = () => {
    const { pathname } = useLocation();
    const [currentDesigns, setCurrentDesigns] = useState<DesignPrincipleType[]>([]);

    useEffect(() => {
        if (pathname.includes("design-principles")) {
            const currentDesignPath = pathname.split("/design-principles/")[1] ?? "";
            const currentDesignLabel = designPrinciples.find((item) => item.route === currentDesignPath)?.label as DesignPrincipleLabel;
            const designs = data.filter((designPrinciple) => designPrinciple.tags.includes(currentDesignLabel));
            setCurrentDesigns(designs);
        } else {
            const currentDesignPath = pathname.split("/section/")[1] ?? "";
            const currentDesignLabel = data.find((item) => item.route === currentDesignPath)?.label;
            const designs = data.filter((designPrinciple) => designPrinciple.label === currentDesignLabel);
            setCurrentDesigns(designs);
        }
    }, [pathname]);

    console.log(currentDesigns);
    return (
        <Box gridArea='content' overflow='scroll' paddingTop='65px'>
            {currentDesigns.map((design) => (
                <Section currentDesign={design} key={design.label} />
            ))}
        </Box>
    );
};

export default DesignPrinciple;
