import { Box, Fade, Flex, Grid, Heading } from "@chakra-ui/react";
import { useLocation } from "react-router-dom";
import Link from "components/Link";
import DesignPrinciple from "./DesignPrinciple";
import { designPrinciples, sections } from "assets/data";
import About from "components/About";

const Root = () => {
    const { pathname } = useLocation();

    return (
        <Grid
            width='100vw'
            height='100vh'
            bgColor='black.100'
            padding='15px'
            overflow='hidden'
            gridTemplateRows='min-content 75px auto'
            gridTemplateColumns='1fr 1fr 1fr 1fr 1fr 1fr'
            gridTemplateAreas={`'title title title about about about' '. . . . . .' 'sidebar content content content content content'`}
            gridColumnGap='15px'
        >
            <Box gridArea='title'>
                <Heading as='h1' size='2xl'>
                    DARK SOULS
                </Heading>
                <Heading as='h3' size='md'>
                    Design Principles Analysis
                </Heading>
            </Box>
            <Flex gridArea='sidebar' flexDir='column' overflow='scroll'>
                <Link to='/about' color={pathname.includes("/about") ? "white.100" : "grey"}>
                    About
                </Link>
                <Link to='/design-principles' color={pathname.includes("/design-principles") ? "white.100" : "grey"}>
                    By Design Principles
                </Link>
                <Link to='/section' color={pathname.includes("/section") ? "white.100" : "grey"} marginBottom='20px'>
                    By Section
                </Link>
                {pathname.includes("/design-principles") && (
                    <Flex flexDir='column' paddingBottom='200px' overflow='scroll' height='100%'>
                        {designPrinciples.map((designPrinciple) => (
                            <Link
                                borderBottom='none'
                                to={`/design-principles/${designPrinciple.route}`}
                                color={pathname.includes(designPrinciple.route) ? "white.100" : "grey"}
                                key={designPrinciple.route}
                            >
                                {designPrinciple.label}
                            </Link>
                        ))}
                    </Flex>
                )}
                {pathname.includes("/section") && (
                    <Fade in={pathname.includes("/section")}>
                        <Flex flexDir='column' paddingBottom='200px' opacity={pathname.includes("/section") ? 1 : 0} overflow='scroll' height='100%'>
                            {sections.map((section) => (
                                <Link
                                    borderBottom='none'
                                    to={`/section/${section.route}`}
                                    color={pathname.includes(section.route) ? "white.100" : "grey"}
                                    key={section.route}
                                >
                                    {section.label}
                                </Link>
                            ))}
                        </Flex>
                    </Fade>
                )}
            </Flex>
            {pathname.includes("/about") ? <About /> : <DesignPrinciple />}
        </Grid>
    );
};

export default Root;
